import numpy as np
import time
import psutil
import sys
import os

board = [
    ['#', '#', '#', "#", '#', '#', '#', "#"],
    ['#', '#', '#', "#", '#', '#', '#', "#"],
    ['#', '#', '#', "#", '#', '#', '#', "#"],
    ['#', '#', '#', "a", 'n', '#', '#', "#"],
    ['#', '#', '#', "n", 'a', '#', '#', "#"],
    ['#', '#', '#', "#", '#', '#', '#', "#"],
    ['#', '#', '#', "#", '#', '#', '#', "#"],
    ['#', '#', '#', "#", '#', '#', '#', "#"],
]


def printBoard():
    print('   0', '1', '2', '3', '4', '5', '6', '7')
    for i in range(len(board)):
        for j in range(len(board)):
            if j == 0:
                print(i, end='| ')
            print(board[i][j], end=" ")
        print()
    print()


def get_opposite_player(currentPlayer):
    if currentPlayer == 'a':
        return 'n'
    return 'a'


def legal_move(currentPlayer, dl, dc):
    opposite_player = get_opposite_player(currentPlayer)
    legal_moves = []
    # Daca nu e libera
    if board[dl][dc] != '#':
        return False
    # in s
    l = dl
    c = dc
    l += 1
    while l < 7:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_l -= 1
            if copy_l == dl:
                break
            while copy_l != dl:
                if board[copy_l][c] != opposite_player:
                    ok = False
                    break
                copy_l -= 1
            if ok is True:
                legal_moves.append('s')
        l += 1
    # in n
    l = dl
    c = dc
    l -= 1
    while l > 0:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_l += 1
            if copy_l == dl:
                break
            while copy_l != dl:
                if board[copy_l][c] != opposite_player:
                    ok = False
                    break
                copy_l += 1
            if ok is True:
                legal_moves.append('n')
        l -= 1
    # in e
    l = dl
    c = dc
    c += 1
    while c < 7:
        ok = True
        if board[l][c] == currentPlayer:
            copy_c = c
            copy_c -= 1
            # Daca gasesc piesa de-a mea fix langa pozitia dorita
            if copy_c == dc:
                break
            while copy_c != dc:
                if board[l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_c -= 1
            if ok is True:
                legal_moves.append('e')
        c += 1
    # in v
    l = dl
    c = dc
    c -= 1
    while c > 0:
        ok = True
        if board[l][c] == currentPlayer:
            copy_c = c
            copy_c += 1
            if copy_c == dc:
                break
            while copy_c != dc:
                if board[l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_c += 1
            if ok is True:
                legal_moves.append('v')
        c -= 1
    # in se
    l = dl
    c = dc
    l += 1
    c += 1
    while l < 7 and c < 7:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_c = c
            copy_l -= 1
            copy_c -= 1
            if copy_l == dl and copy_c == dc:
                break
            while copy_l != dl and copy_c != dc:
                if board[copy_l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_l -= 1
                copy_c -= 1
            if ok is True:
                legal_moves.append('se')
        l += 1
        c += 1
    # in sv
    l = dl
    c = dc
    l += 1
    c -= 1
    while l < 7 and c > 0:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_c = c
            copy_l -= 1
            copy_c += 1
            if copy_l == dl and copy_c == dc:
                break
            while copy_l != dl and copy_c != dc:
                if board[copy_l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_l -= 1
                copy_c += 1
            if ok is True:
                legal_moves.append('sv')
        l += 1
        c -= 1
    # in ne
    l = dl
    c = dc
    l -= 1
    c += 1
    while l > 0 and c < 7:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_c = c
            copy_l += 1
            copy_c -= 1
            if copy_l == dl and copy_c == dc:
                break
            while copy_l != dl and copy_c != dc:
                if board[copy_l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_l += 1
                copy_c -= 1
            if ok is True:
                legal_moves.append('ne')
        l -= 1
        c += 1
    # in nv
    l = dl
    c = dc
    l -= 1
    c -= 1
    while l > 0 and c > 0:
        ok = True
        if board[l][c] == currentPlayer:
            copy_l = l
            copy_c = c
            copy_l += 1
            copy_c += 1
            if copy_l == dl and copy_c == dc:
                break
            while copy_l != dl and copy_c != dc:
                if board[copy_l][copy_c] != opposite_player:
                    ok = False
                    break
                copy_l += 1
                copy_c += 1
            if ok is True:
                legal_moves.append('nv')
        l -= 1
        c -= 1

    return  legal_moves


def occupy_opponent(currentPlayer, dl, dc, legal_moves):
    global board
    # legal_moves is not False
    l = dl
    c = dc
    for elem in legal_moves:
        if elem == 's':
            l += 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l += 1
            # reset position
            l = dl
            c = dc
        if elem == 'n':
            l -= 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l -= 1
            l = dl
            c = dc
        if elem == 'e':
            c += 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                c += 1
            l = dl
            c = dc
        if elem == 'v':
            c -= 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                c -= 1
            l = dl
            c = dc
        if elem == 'se':
            l += 1
            c += 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l += 1
                c += 1
            l = dl
            c = dc
        if elem == 'sv':
            l += 1
            c -= 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l += 1
                c -= 1
            l = dl
            c = dc
        if elem == 'ne':
            l -= 1
            c += 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l -= 1
                c += 1
            l = dl
            c = dc
        if elem == 'nv':
            l -= 1
            c -= 1
            while board[l][c] != currentPlayer:
                board[l][c] = currentPlayer
                l -= 1
                c -= 1
            l = dl
            c = dc


def calculateScore():
    contor_a = 0
    contor_n = 0
    for i in range(len(board)):
        for j in range(len(board)):
            if board[i][j] == 'n':
                contor_n += 1
            elif board[i][j] == 'a':
                contor_a += 1
    return contor_a - contor_n


def has_any_possible_move(currentPlayer):
    # Are there any more moves?
    for i in range(len(board)):
        for j in range(len(board)):
            possibilities = legal_move(currentPlayer, i, j)
            # check if move is not occupied and move is valid
            if possibilities is not False and possibilities:
                return True
    return False


def bestMove_mm():
    global currentPlayer, board
    bestScore = -500000
    move = None
    directii = []
    for i in range(0, 7):
        for j in range(0, 7):
            # TEST if it's a valid move
            possibilities = legal_move(currentPlayer, i, j)
            # check if move is not occupied and move is valid
            if possibilities is not False and possibilities:
                board[i][j] = ai
                score = max(bestScore, minimax(board, depth, True))
                if score == bestScore:
                    continue
                board[i][j] = '#'
                move = (i, j)
                directii = possibilities
    board[move[0]][move[1]] = ai
    occupy_opponent(currentPlayer, move[0], move[1], directii)
    currentPlayer = human


def bestMove_ab():
    global currentPlayer, board
    bestScore = -500000
    move = None
    directii = []
    alfa = -500000
    beta = 500000
    for i in range(0, 7):
        for j in range(0, 7):
            # TEST if it's a valid move
            possibilities = legal_move(currentPlayer, i, j)
            # check if move is not occupied and move is valid
            if possibilities is not False and possibilities:
                board[i][j] = ai
                score = max(bestScore, alphabeta(board, depth, alfa, beta, True))
                if score == bestScore:
                    continue
                board[i][j] = '#'
                move = (i, j)
                directii = possibilities
    board[move[0]][move[1]] = ai
    occupy_opponent(currentPlayer, move[0], move[1], directii)
    currentPlayer = human


def minimax(board, depth, isMaximizing):
    # result 0 daca e remiza, > 0 daca a castigat 'a'
    result = calculateScore()
    # Daca s-a terminat jocul sau depth = 0
    if result != 0 or depth == 0:
        return result

    if isMaximizing:
        bestScore = -500000
        for i in range(0, 7):
            for j in range(0, 7):
                # TEST if it's a valid move
                possibilities = legal_move(currentPlayer, i, j)
                # check if move is not occupied and move is valid
                if possibilities is not False and possibilities:
                    board[i][j] = ai
                    score = max(bestScore, minimax(board, depth - 1, False))
                    board[i][j] = '#'
        # return heuristic value
        return bestScore
    else:
        bestScore = 500000
        for i in range(0, 7):
            for j in range(0, 7):
                # TEST if it's a valid move
                possibilities = legal_move(currentPlayer, i, j)
                # check if move is not occupied and move is valid
                if possibilities is not False and possibilities:
                    board[i][j] = human
                    score = min(bestScore, minimax(board, depth - 1, True))
                    board[i][j] = '#'
        # return heuristic value
        return bestScore


def alphabeta(board, depth, alpha, beta, isMaximizing):
    # result 0 daca e remiza, > 0 daca a castigat 'a'
    result = calculateScore()
    # Daca s-a terminat jocul sau depth = 0
    if result != 0 or depth == 0:
        return result

    if isMaximizing:
        bestScore = -500000
        for i in range(0, 7):
            for j in range(0, 7):
                # TEST if it's a valid move
                possibilities = legal_move(currentPlayer, i, j)
                # check if move is not occupied and move is valid
                if possibilities is not False and possibilities:
                    board[i][j] = ai
                    score = max(bestScore, minimax(board, depth - 1, False))
                    alpha = max(alpha, score)
                    board[i][j] = '#'
                    if alpha >= beta:
                        break
        # return heuristic value
        return bestScore
    else:
        bestScore = 500000
        for i in range(0, 7):
            for j in range(0, 7):
                # TEST if it's a valid move
                possibilities = legal_move(currentPlayer, i, j)
                # check if move is not occupied and move is valid
                if possibilities is not False and possibilities:
                    board[i][j] = human
                    score = min(bestScore, minimax(board, depth - 1, True))
                    beta = min(beta, score)
                    board[i][j] = '#'
                    if alpha >= beta:
                        break
        # return heuristic value
        return bestScore


def checkWinner(currentPlayer):
    # Daca jucatorul curent nu poate muta, se schimba jucatorul curent
    if has_any_possible_move(currentPlayer) is False:
        currentPlayer = get_opposite_player(currentPlayer)
        # Daca nici actualul jucator nu poate muta, se termina jocul
        if has_any_possible_move(currentPlayer) is False:
            print("End Game")
            score = calculateScore()
            if score == 0:
                print('Remiza!')
            if score > 0:
                print("A castigat 'a'!")
            else:
                print("A castigat 'n'!")
            return True
    return False


while True:
    try:
        algorithm = input("Your desired algorithm (mm/ab): ")
        human = input("Your desired color (a/n): ")
        difficulty = input("Difficulty (i/m/a): ")
        if algorithm not in ['mm', 'ab'] or human not in ['a', 'n'] or difficulty not in ['i', 'm', 'a']:
            raise Exception('Wrong input')
        break
    except Exception as e:
        print(e)

ai = get_opposite_player(human)
if human == 'n':
    currentPlayer = human
else:
    currentPlayer = ai

if difficulty == 'i':
    depth = 6
elif difficulty == 'm':
    depth = 7
elif difficulty == 'a':
    depth = 8

while True:
    if checkWinner(currentPlayer) is True:
        break
    # Daca jucatorul curent are cel putin o miscare posibila, joaca

    printBoard()
    if currentPlayer == human:
        print('Your turn')
        print('Continue? Y/N')
        con = input()

        if con == 'Y':
            while True:
                line = int(input("line "))
                # line = 5
                column = int(input("column "))
                # column = 4
                # TEST if it's a valid move
                possibilities = legal_move(currentPlayer, line, column)
                # check if move is not occupied and move is valid
                if possibilities is not False and possibilities:
                    board[line][column] = currentPlayer
                    occupy_opponent(currentPlayer, line, column, possibilities)
                    currentPlayer = ai
                    break
                print("Not a legal position, try again!")
        # Daca nu mai doreste sa continue jocul
        else:
            scor_partial = calculateScore()
            if scor_partial == 0:
                print('Scor egal!')
            if scor_partial > 0:
                print(" 'a' are scor mai mare!")
            else:
                print(" 'n' are scor mai mare!'")
            break
    else:
        print('AI\'s turn')

        time1 = time.time()

        if algorithm == 'mm':
            bestMove_mm()
        else:
            bestMove_ab()

        time2 = time.time()
        process = psutil.Process(os.getpid())
        memorie = process.memory_info()[0]
        sec = (time2 - time1)
        print("Timp de gandire pentru mutarea curenta: ", sec)
        print("Memorie utilizata pentru mutarea curenta: ", memorie)
